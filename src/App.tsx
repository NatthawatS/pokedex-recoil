import { FC } from "react";
import { RecoilRoot } from "recoil";
import Layout from "./components/Layout";
import Routes from "./config/routes";

const App: FC = () => {
  return (
    <RecoilRoot>
      <Layout>
        <Routes />
      </Layout>
    </RecoilRoot>
  );
};

export default App;
