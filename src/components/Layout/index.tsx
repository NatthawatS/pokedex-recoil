import styled from "@emotion/styled";
import React from "react";
import useThemesColorsHook from "../../hook/useThemesColors.hook";
import { breakpoint, size } from "../../styles/breakpoint";

export const Desktop = styled.div`
  display: none;
  @media ${breakpoint.desktop} {
    display: block;
  }
`;

export const Mobile = styled.div`
  display: block;
  @media ${breakpoint.desktop} {
    display: none;
  }
`;

export const Container = styled.div`
  display: grid;
  grid-auto-rows: max-content;
  max-width: ${size.desktop}px;
  padding: 1.25rem;
  margin: 0 auto;
  @media ${breakpoint.desktop} {
    min-height: 70vh;
  }
`;

export interface BgColorsProps {
  bgColors: string;
}

const Background = styled.div<BgColorsProps>`
  background-color: ${(props) =>
    props.bgColors === "light" ? "#FCF8EC" : "#456268"};
  height: max-content;
  background-size: 400% 400%;
  min-height: 100vh;
`;

export const Layout: React.FC = ({ children }) => {
  const { themesColors } = useThemesColorsHook();

  return (
    <>
      {/* <Header /> */}
      <Background bgColors={themesColors.colors}>{children}</Background>
    </>
  );
};

export default Layout;
