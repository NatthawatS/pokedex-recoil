import { useRecoilState } from "recoil";
import themesColorsAtom from "../recoil/themesColors";

const useThemesColorsHook = () => {
  const [themesColors, setThemesColors] = useRecoilState(themesColorsAtom);

  const handleChangeThemesColors = (value: string) => {
    setThemesColors({ colors: value });
  };
  return {
    themesColors,
    handleChangeThemesColors,
  };
};

export default useThemesColorsHook;
