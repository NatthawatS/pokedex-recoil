import useThemesColorsHook from "../../hook/useThemesColors.hook";
import { Container } from "./home.styled";

const Home: React.FC = () => {
  const { handleChangeThemesColors } = useThemesColorsHook();
  return (
    <Container>
      <button onClick={() => handleChangeThemesColors("light")}>light</button>
      <button onClick={() => handleChangeThemesColors("dark")}>dark</button>
    </Container>
  );
};

export default Home;
