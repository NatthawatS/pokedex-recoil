import { atom } from "recoil";

import { ThemesColorsProps } from "./types";

const themesColors = {
  colors: "light",
} as ThemesColorsProps;

const themesColorsAtom = atom({
  key: "themesColorsAtom",
  default: themesColors,
});

export default themesColorsAtom;
